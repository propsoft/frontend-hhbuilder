// vars
var form = document.forms[0],
    age = form['age'],
    rel = form['rel'],
    smoker = form['smoker'],
    debug = document.getElementsByClassName('debug')[0],
    addBtn = document.getElementsByClassName('add')[0],
    memberList = document.getElementsByClassName('household')[0],
    members = [],
    errors = document.createElement('div')

// add id and styling for errors
errors.id = 'errors'
errors.style.color = 'red'

// allow only numbers in the age field
age.setAttribute('type', 'number')

// hijack default submit form
form.addEventListener('submit', submitHandler)

// create event listener for add button
addBtn.addEventListener('click', addMember)

// validate form values
function valForm() {
  var agevalue = Number(age.value)

  switch (true) {
    case !agevalue:
      age.parentNode.appendChild(errors)
      errors.innerHTML = 'An age is required.'
      age.focus()
      return false

    case agevalue < 1:
      age.parentNode.appendChild(errors)
      errors.innerHTML = 'An age older than 0 is required.'
      age.focus()
      return false

    case (agevalue - Math.floor(agevalue)) !== 0:
      age.parentNode.appendChild(errors)
      errors.innerHTML = 'A whole number is required.'
      age.focus()
      return false

    case !rel.value:
      rel.parentNode.appendChild(errors)
      errors.innerHTML = 'Relationship type is required.'
      rel.focus()
      return false

    default:
      return true
  }

}

// add member function
function addMember(e) {
  e.preventDefault()

  // if form is valid add member to array
  if (valForm()) {

    // add member to members array to submit to server
    members.push({
      age: age.value,
      relationship: rel.value,
      smoker: smoker.checked
    })

    var html = '<p>Age: ' + age.value + '</p>'
        html += '<p>Relationship: ' + rel.value + '</p>'
        html += '<p>Smoker: ' + smoker.checked + '</p>'
        html += '<p><button onclick="removeMember()">remove</button></p>',
        member = document.createElement('li')

    member.innerHTML = html

    memberList.appendChild(member)

    age.focus()

    resetForm()

  }
}

// reset form to init state
function resetForm() {
  if(document.getElementById('errors')){
    document.getElementById('errors').remove()
  }
  form.reset()
}

// remove member function
function removeMember() {
  for (var i = 0; i < memberList.children.length; i++) {

    (function (index) {
      memberList.children[i].onclick = function () {
        members.splice(index, 1)
        memberList.removeChild(memberList.childNodes[index])
        if (!members.length) debug.style = 'display: none'
      }
    })(i);

  }
}

// form submit handler
function submitHandler(e) {
  e.preventDefault()
  if (members.length > 0) {
    debug.innerHTML = JSON.stringify(members)
    debug.style = 'display: block'
    resetForm()
  } else {
    form.parentNode.appendChild(errors)
    errors.innerHTML = 'You must add at leaset one household member to your list.'
  }
}
